import configparser
import os
import socket
import struct
import json

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
parser = configparser.ConfigParser()
parser.read(THIS_FOLDER + '/clientconfig.cfg')

SERVER_IP = parser['SERVERDATA']['ip']
SERVER_PORT = int(parser['SERVERDATA']['port'])
MAX_MSG_SIZE = int(parser['MESSAGEDATA']['max_len'])

LOGIN_REQUEST = int(parser['REQUESTS']['login_request'])
SIGNUP_REQUEST = int(parser['REQUESTS']['signup_request'])

class Client:
    username = str()
    password = str()
    email = str()
    
    def __init__(self, username: str, password: str, email: str = ""):
        self.username = username
        self.password = password
        self.email = email

    def __str__(self):
        return "Client Details:\n\tusername: {}\n\temail: {}\n\tpassword: {}\n******************************".format(self.username, self.email, self.password)
    

def connect():
    # Create a TCP/IP socket

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = (SERVER_IP, SERVER_PORT)

    sock.connect(server_address)

    return sock


def login():
    c = Client(input("Enter username: "), input("Enter password: "))
    msg_dict = {'username' : c.username, 'password' : c.password}
    print(c)
    return c, msg_dict

def signup():
    c = Client(input("Enter username: "), input("Enter password: "),input("Enter eMail: "))
    msg_dict = {'username' : c.username, 'password' : c.password, 'email' : c.email}
    print(c)
    return c, msg_dict

def send_to_server_and_get_response(sock: socket,msg_json_str: str, msg_type: int) -> dict:
    msg = msg_type.to_bytes(1, 'big') + len(msg_json_str).to_bytes(4, 'big') + msg_json_str.encode()
    print("Sending Message: {}".format(msg))
    sock.sendall(msg)

    server_msg = sock.recv(MAX_MSG_SIZE)
    server_msg = server_msg.decode()
    server_msg_json = parse_server_msg(server_msg) # can return json but rather see the entire response.
    return server_msg

def main():
    sock = connect()
    c = None # Declare a variable with no type to later init as a class instance.

    print("If you're here it means your'e connected to the server")
    msg_type = int(input("1 for signup or 2 for login: "))
    msg_dict = dict()

    while msg_type != 1 and msg_type != 2:
        msg_type = int(input("1 for signup or 2 for login: "))

    if msg_type == 1:
        msg_type = SIGNUP_REQUEST
        c, msg_dict = signup()
    elif msg_type == 2:
        msg_type = LOGIN_REQUEST
        c, msg_dict = login()

    print(str(send_to_server_and_get_response(sock, json.dumps(msg_dict), msg_type)))


def parse_server_msg(msg: str) -> dict:
    return dict(json.loads(msg[41:]))

if __name__ == '__main__':
    while True:
        try:
            if input("(To try to connect to the server press enter or type 'exit' to exit~>").lower() == 'exit':
                input("Goodbye!")
                break
            else:
                main()
        except Exception as e:
            print("*********************\n{}\n*********************".format(e))