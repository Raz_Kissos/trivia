#pragma once
#include <string>

#define LOGIN_REQUEST 100
#define SIGNUP_REQUEST 200


typedef struct LoginRequest {
	std::string username;
	std::string password;
}LoginRequest;

typedef struct SignupRequest {
	std::string username;
	std::string password;
	std::string email;
}SignupRequest;
