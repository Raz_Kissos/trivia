#pragma once
#include <string>

#define LOGIN_MESSAGE_CODE 101
#define SIGNUP_MESSAGE_CODE 202
#define ERROR_MESSAGE_CODE 404

#define SIGNUP_LOGIN_LENGTH 13 
#define ERROR_LENGTH 20

typedef struct LoginResponse {
	unsigned int status = 0; //1 for success, 0 for failure.
}LoginResponse;

typedef struct SignupResponse {
	unsigned int status = 0; //1 for success, 0 for failure.
}SignupResponse;

typedef struct ErrorResponse {
	std::string message = "ERROR";
}ErrorResponse;