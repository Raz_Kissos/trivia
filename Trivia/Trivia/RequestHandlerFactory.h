#pragma once
#include "LoginRequestHandler.h"
#include "SqliteDataBase.h"
#include "LoginManager.h"
#include "IDataBase.h"

class LoginRequestHandler;
class SqliteDataBase;
class LoginManager;
class IDataBase;
class RequestHandlerFactory
{
private:
	IDatabase* _db;
	LoginManager* _loginManager;
	static RequestHandlerFactory* instance;
	
	RequestHandlerFactory();
	~RequestHandlerFactory();

public:
	
	static RequestHandlerFactory* getInstance(void);
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager* getLoginManager();
};
