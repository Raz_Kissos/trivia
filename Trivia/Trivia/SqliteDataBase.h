#pragma once
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include "sqlite3.h"
#include "IDatabase.h"

#define _CRT_SECURE_NO_WARNINGS

class SqliteDataBase : public IDatabase
{
private:
	static sqlite3* db;
	static char* zErrMsg;
	static int rc;
	static int callback_FBF(void* a, int argc, char** argv, char** azColName);
	static int callback(void* notUsed, int argc, char** argv, char** azColName);
	static bool sendQuery_FBF(std::string sql);
	static void sendQuery(std::string sql);

public:
	virtual bool isUserExist(std::string username);
	virtual bool doesPasswordMatch(std::string username, std::string password);
	virtual bool addUser(std::string username, std::string password, std::string email);
};

