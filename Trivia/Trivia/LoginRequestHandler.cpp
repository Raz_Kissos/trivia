#include "LoginRequestHandler.h"

RequestResult LoginRequestHandler::login(RequestInfo info)
{
	LoginRequest loginReq;
	bool status = false; // True if request successful.
	loginReq = JsonRequestPacketDeserializer::DeserializeLoginRequest(info.buffer);
	status = h_factory->getLoginManager()->login(loginReq.username, loginReq.password);
	RequestResult res;
	std::string buffer;

	if (status) // Login Successful.
	{
		buffer = JsonResponsePacketSerializer::serializeResponse(LoginResponse{ 1 });
		res = RequestResult{ Helper::chArrToBuffer(buffer.c_str(), buffer.length()), nullptr }; //nullptr for now, will change when adding new handlers.
		std::cout << "*~* Client " << loginReq.username << " Successfully Logged In!" << std::endl;
	}
	else
	{
		buffer = JsonResponsePacketSerializer::serializeResponse(LoginResponse{ 0 });
		res = RequestResult{ Helper::chArrToBuffer(buffer.c_str(), buffer.length()) , new LoginRequestHandler() };
	}

	return res;
}	

RequestResult LoginRequestHandler::signup(RequestInfo info)
{
	SignupRequest signupReq;
	bool status = false; // True if request successful.
	signupReq = JsonRequestPacketDeserializer::DeserializeSignupRequest(info.buffer);
	status = h_factory->getLoginManager()->signup(signupReq.username, signupReq.password, signupReq.email);
	RequestResult res;
	std::string buffer;

	if (status) // Signup Successful.
	{
		buffer = JsonResponsePacketSerializer::serializeResponse(SignupResponse{ 1 });
		res = RequestResult{ Helper::chArrToBuffer(buffer.c_str(), buffer.length()), nullptr }; //nullptr for now, will change when adding new handlers.
		std::cout << "*~* Client " << signupReq.username << " Successfully Signed Up!" << std::endl;
	}
	else
	{
		buffer = JsonResponsePacketSerializer::serializeResponse(SignupResponse{ 0 });
		res = RequestResult{ Helper::chArrToBuffer(buffer.c_str(), buffer.length()) , new LoginRequestHandler() };
	}

	return res;
}

LoginRequestHandler::LoginRequestHandler() : h_factory(RequestHandlerFactory::getInstance())
{
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
	int reqCode = (int)(info.buffer[0]);
	return (reqCode == LOGIN_REQUEST || reqCode == SIGNUP_REQUEST);
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
	int msgType = Helper::getMsgCode(info.buffer);

	switch (msgType)
	{
	case LOGIN_REQUEST:
		return this->login(info);
		break;
	case SIGNUP_REQUEST:
		return this->signup(info);
		break;
	}
}