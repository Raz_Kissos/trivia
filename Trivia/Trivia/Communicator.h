#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <thread>
#include <mutex>
#include <map>

#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "LoginRequestHandler.h"
#include "IRequestHandler.h"
#include "Server.h"
#include "Helper.h"

#define MAX_MSG_SIZE 10004 //5 + 9999

class Server;
class Communicator
{
public:
    static Communicator* getInstance(void);
    void startHandleRequests(SOCKET server_socket);

private:
    Communicator();
    ~Communicator();
    static Communicator* instance; // for singleton implementation.
    std::mutex users_m;
    SOCKET server_socket;
    std::map<SOCKET, IRequestHandler*> _clients;
    RequestHandlerFactory* _handlerFactory = RequestHandlerFactory::getInstance();

    //Functions:
    void bindAndListen();
    void handleNewClient(SOCKET s);
};