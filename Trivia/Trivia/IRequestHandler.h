#pragma once
#include "HandlersStructs.h"
#include "ResponseStructs.h"
#include "RequstStructs.h"

struct RequestResult;
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo info) = 0;
	virtual RequestResult handleRequest(RequestInfo info) = 0;
};