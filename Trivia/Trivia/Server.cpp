#include "Server.h"
Server* Server::instance = 0;
Server::Server() : _communicator(Communicator::getInstance()), _db(new SqliteDataBase())
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_server_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket error");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_server_socket);
	}
	catch (...) {}
}

Server* Server::getInstance()
{
	if (instance == nullptr)
		instance = new Server();

	return instance;
}

void Server::run()
{
	// Printing startup data.
	printf("Owners: Raz Kissos and Noam Didi.\n");
	time_t cur_time = time(NULL);
	char time_str[26];
	ctime_s(time_str, sizeof time_str, &cur_time);
	printf("%s\n", time_str);
	printf("Trivia Server Running...\n");
	printf("----------------------------------\n");

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_server_socket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind error");

	// Start listening for incoming requests of clients
	if (listen(_server_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen error");

	try {
		std::thread t_connector(&Communicator::startHandleRequests, _communicator, (this->_server_socket));
		t_connector.detach();
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	

	std::string console_input = "";

	while (console_input != "exit")
	{
		// the main thread is only server console input. 
		std::cin >> console_input;
	}
}