#pragma comment(lib, "ws2_32.lib")
#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include "JsonResponsePacketSerializer.h" 
#include "RequstStructs.h"

int main(void)
{
	try {
		WSAInitializer wsainit = WSAInitializer();
		Server* s = Server::getInstance();
		s->run();
	}
	catch (std::exception& e){
		std::cout << e.what() << std::endl;
	}

	return 0;
}
