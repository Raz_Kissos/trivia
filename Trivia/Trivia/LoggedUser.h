#include <iostream>

#pragma once
class LoggedUser
{
private:
	std::string _username;

public:
	LoggedUser(std::string username);
	std::string getUsername();
};

