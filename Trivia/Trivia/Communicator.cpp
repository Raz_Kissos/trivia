#include "Communicator.h"

Communicator* Communicator::instance = 0;
Communicator::Communicator()
{
	this->server_socket = NULL;
}

Communicator::~Communicator()
{
	this->_clients.clear();
}

Communicator* Communicator::getInstance(void)
{
	if (instance == nullptr)
		instance = new Communicator();
	return instance;
}

void Communicator::startHandleRequests(SOCKET server_socket)
{
	this->server_socket = server_socket;
	while (true)
	{
		this->bindAndListen();
	}
}

void Communicator::bindAndListen()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from the server to this client
	SOCKET client_socket = accept(this->server_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "*~* Client accepted." << std::endl;
	// the function that handles the conversation with the client
	std::thread client_thread(&Communicator::handleNewClient, this, client_socket);
	client_thread.detach();
}

void Communicator::handleNewClient(SOCKET s)
{
	std::pair<SOCKET, LoginRequestHandler*> p(s, this->_handlerFactory->createLoginRequestHandler());
	std::unique_lock<std::mutex> users_lk(users_m);
	this->_clients.insert(p);
	users_lk.unlock();
	
	std::vector<unsigned char> buffer;
	while (true)
	{
		try {
			buffer = Helper::getBuffer(s);
		}
		catch (std::exception & e) {
			std::cout << e.what() << "Client Disconnected Violently!" << std::endl;
		}

		try {
			RequestResult res = {};
			users_lk.lock();
			res = (this->_clients.find(s)->second)->handleRequest(RequestInfo{ Helper::getMsgCode(buffer), time(NULL), buffer });
			users_lk.unlock();

			Helper::sendData(s, Helper::BufferToStr(res.buffer));
		}
		catch (std::exception & e)
		{
			Helper::sendData(s, JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ e.what() }));
		}
	}
}
