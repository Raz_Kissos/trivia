#pragma once

#include <stdio.h>
#include <iostream>
#include <string>
#include <WinSock2.h>
#include "Helper.h"
#include "ResponseStructs.h"
#include "json.hpp"
#include <bitset>

static class JsonResponsePacketSerializer
{
public:
	static std::string serializeResponse(ErrorResponse er);
	static std::string serializeResponse(LoginResponse lr);
	static std::string serializeResponse(SignupResponse sr);
};

