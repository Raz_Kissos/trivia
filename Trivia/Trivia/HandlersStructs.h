#pragma once
#include <ctime>
#include <vector>
#include "IRequestHandler.h"

class IRequestHandler; //To prevent bug in decleration.
typedef struct RequestInfo {
	int reqId; // The request code.
	time_t receivalTime;
	std::vector<unsigned char> buffer; // The request itself.
}RequestInfo;

typedef struct RequestResult{
	std::vector<unsigned char> buffer; // Our serialized response.
	IRequestHandler* newHandler; // The new handler for the client.
}RequestResult;