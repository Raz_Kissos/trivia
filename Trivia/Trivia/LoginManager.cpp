#include "LoginManager.h"
LoginManager* LoginManager::instance = 0;
LoginManager::LoginManager() : sdb(SqliteDataBase())
{
}

LoginManager* LoginManager::getInstance()
{
	if (instance == nullptr)
		instance = new LoginManager();

	return instance;
}

bool LoginManager::signup(std::string username, std::string password, std::string email)
{
	return sdb.addUser(username, password, email);
}

bool LoginManager::login(std::string username, std::string password)
{
	if (sdb.isUserExist(username))
	{
		if (sdb.doesPasswordMatch(username, password))
		{
			LoggedUser l = LoggedUser(username);
			this->m_loggedUsers.push_back(l);
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

bool LoginManager::logout(std::string username)
{
	for (std::vector<LoggedUser>::iterator it = this->m_loggedUsers.begin(); it != this->m_loggedUsers.begin(); ++it)
	{
		if (it->getUsername() == username)
		{
			this->m_loggedUsers.erase(it);
			return true;
		}
	}
	return false;
}


