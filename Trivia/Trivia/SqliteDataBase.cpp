#include "SqliteDataBase.h"

char* SqliteDataBase::zErrMsg = 0;
sqlite3* SqliteDataBase::db = 0;
int SqliteDataBase::rc = 0;

int SqliteDataBase::callback_FBF(void* a, int argc, char** argv, char** azColName)
{
	bool* aPtr = (bool*)a;
	*aPtr = true;
	printf("USER EXIST");
	return 0;
}

int SqliteDataBase::callback(void* notUsed, int argc, char** argv, char** azColName)
{
	int i;
	for (i = 0; i < argc; i++)
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	printf("\n");
	return 0;
}

bool SqliteDataBase::sendQuery_FBF(std::string sql)
{
	bool flag = false;
	rc = sqlite3_exec(db, sql.c_str(), callback_FBF, &flag, &zErrMsg);

	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else 
		fprintf(stdout, "\nACTION COMPLETED SUCCESSFULLY!\n");

	return flag; //True if successful, else: false.
}

void SqliteDataBase::sendQuery(std::string sql)
{
	int a = 0;
	rc = sqlite3_exec(db, sql.c_str(), callback, 0, &(zErrMsg));

	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else 
		fprintf(stdout, "\nACTION COMPLETED SUCCESSFULLY!\n");
}

bool SqliteDataBase::isUserExist(std::string username)
{
	rc = sqlite3_open("TriviaUsers.db", &db);

	if (rc) 
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	else 
		fprintf(stdout, "Opened database successfully\n");

	std::string sql = "SELECT USERNAME FROM USERS WHERE USERNAME = '" + username + "'; ";
	bool f = sendQuery_FBF(sql);
	sqlite3_close(db);

	return f;
}

bool SqliteDataBase::doesPasswordMatch(std::string username, std::string password)
{
	rc = sqlite3_open("TriviaUsers.db", &db);

	if (rc) 
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	else 
		fprintf(stdout, "Opened database successfully\n");

	std::string sql = "SELECT USERNAME FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "'; ";
	bool f = false;
	sendQuery_FBF(sql);

	sqlite3_close(db);
	return f;
}

bool SqliteDataBase::addUser(std::string username, std::string password, std::string email)
{
	rc = sqlite3_open("TriviaUsers.db", &db);

	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return false;
	}
	else 
		fprintf(stdout, "Opened database successfully\n");

	std::string sql = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL) VALUES ('" + username + "', '" + password + "', '" + email + "');";
	sendQuery(sql);
	sqlite3_close(db);
	return true;
}
