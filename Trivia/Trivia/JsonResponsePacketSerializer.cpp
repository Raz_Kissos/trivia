#include "JsonResponsePacketSerializer.h"

std::string JsonResponsePacketSerializer::serializeResponse(ErrorResponse er)
{
	std::string buffer = { 0 };
	std::string messageCode = std::bitset<8>(ERROR_MESSAGE_CODE).to_string(); // 8 bits = 1 byte
	std::string messageLength = std::bitset<32>(ERROR_LENGTH).to_string(); // 32 bits = 4 bytes

	nlohmann::json jMessage;
	jMessage["status"] = er.message;
	
	buffer += messageCode;
	buffer += messageLength;
	buffer += jMessage.dump();

	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(LoginResponse lr)
{
	std::string buffer = { 0 };
	std::string messageCode = std::bitset<8>(LOGIN_MESSAGE_CODE).to_string(); // 8 bits = 1 byte
	std::string messageLength = std::bitset<32>(SIGNUP_LOGIN_LENGTH).to_string(); // 32 bits = 4 bytes

	nlohmann::json jMessage;
	jMessage["status"] = std::to_string(lr.status);
	
	buffer += messageCode;
	buffer += messageLength;
	buffer += jMessage.dump();
	
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(SignupResponse sr)
{
	std::string buffer = { 0 };
	std::string messageCode = std::bitset<8>(SIGNUP_MESSAGE_CODE).to_string(); // 8 bits = 1 byte
	std::string messageLength = std::bitset<32>(SIGNUP_LOGIN_LENGTH).to_string(); // 32 bits = 4 bytes

	nlohmann::json jMessage;
	jMessage["status"] = std::to_string(sr.status);

	buffer += messageCode;
	buffer += messageLength;
	buffer += jMessage.dump();

	return buffer;
}