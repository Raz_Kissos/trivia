#pragma once
#include "RequestHandlerFactory.h"
#include "IDatabase.h"
#include "Communicator.h"
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <ctime>
#include <thread>

#define PORT 888

class RequestHandlerFactory;
class IDatabase;
class Communicator;
class Server
{
public:
	static Server* getInstance(void);
	void run();

private:
	Server();
	~Server();
	static Server* instance; // for singleton implementation.
	SOCKET _server_socket;
	Communicator* _communicator;

	IDatabase* _db;
	RequestHandlerFactory* _handlerFactory = RequestHandlerFactory::getInstance();
};