#pragma once
#include <vector>
#include <iostream>
#include "Helper.h"
#include "json.hpp"
#include "RequstStructs.h"

class Helper;
static class JsonRequestPacketDeserializer
{
public:
	static LoginRequest DeserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignupRequest DeserializeSignupRequest(std::vector<unsigned char> buffer);

private:
	static bool isRequestValid(std::vector<unsigned char> buffer, int reqType);
};

