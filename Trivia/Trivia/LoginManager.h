#pragma once

#include <iostream>
#include <vector>
#include <string>
#include "IDatabase.h"
#include "SqliteDataBase.h"
#include "LoggedUser.h"

class LoginManager
{
private:
	SqliteDataBase sdb;
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
	static LoginManager* instance;
	LoginManager();

public:
	static LoginManager* getInstance();
	bool signup(std::string username, std::string password, std::string email);
	bool login(std::string username, std::string password);
	bool logout(std::string username);
};

