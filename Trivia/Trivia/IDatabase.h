#pragma once

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include "sqlite3.h"

class IDatabase
{
public: 
	virtual bool isUserExist(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string username, std::string password) = 0;
	virtual bool addUser(std::string username, std::string password, std::string email) = 0;
};

