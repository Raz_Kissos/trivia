#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

using std::string;

std::string Helper::toBinary(int n)
{
	std::string r;
	while (n != 0) { r = (n % 2 == 0 ? "0" : "1") + r; n /= 2; }
	return r;
}

//void Helper::pad(char* s, int n, int c)
//{
//	char* p = s + n - strlen(s);
//	strcpy(p, s);
//	p--;
//	while (p >= s)
//	{
//		p[0] = c; p--;
//	}
//}

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getTypeCodeFromMessage(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 1);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}

// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "*~* Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

std::vector<unsigned char> Helper::getBuffer(SOCKET sc)
{
	std::vector<unsigned char> toRet;
	char* lenAndSize = getPartFromSocket(sc, 5);
		
	toRet.push_back((unsigned char)lenAndSize[0]);
	for (int i = 1; i < 5; i++)
		toRet.push_back((unsigned char)lenAndSize[i]);
	int messageSize = Helper::getMsgLen(toRet);
	char* restOfRequest = getPartFromSocket(sc, messageSize);
	for (int i = 0; i < messageSize; i++)
		toRet.push_back((unsigned char)restOfRequest[i]);

	return toRet;
}

// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("*~* Error while sending message to client");
	}
}

std::vector<unsigned char> Helper::chArrToBuffer(const char* buffer, int len)
{
	std::vector<unsigned char> toRet;

	for (int i = 0; i < len; i++)
		toRet.push_back((unsigned char)buffer[i]);

	return toRet;
}

int Helper::getMsgCode(std::vector<unsigned char> buffer)
{
	return (int)buffer[0];
}

int Helper::getMsgLen(std::vector<unsigned char> buffer)
{
	int reqLen = buffer[1];
	for (size_t i = 2; i < 5; i++)
		reqLen = (reqLen * 256) + (int)buffer[i];

	return reqLen;
}

std::string Helper::BufferToStr(std::vector<unsigned char> buffer)
{
	int index = 0;
	std::vector<char> charBuff;
	while (index < buffer.size())
		charBuff.push_back((char)buffer[index++]);

	return std::string (charBuff.begin(), charBuff.end());
}
