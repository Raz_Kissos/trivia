#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::DeserializeLoginRequest(std::vector<unsigned char> buffer)
{
	if (!isRequestValid(buffer, LOGIN_REQUEST))
	{
		std::string errMsg = "Login request not valid!";
		throw std::exception(errMsg.c_str());
	}
	int reqLen = Helper::getMsgLen(buffer);
	int index = 0;
	std::vector<char> charReq;
	while (index < reqLen)
		charReq.push_back((char)buffer[(index++) + 5]);

	std::string toJson(charReq.begin(), charReq.end());
	nlohmann::json j = nlohmann::json::parse(toJson);
	if (j.find("username") != j.end() && j.find("password") != j.end())
		return LoginRequest { j["username"] , j["password"] };
	else
	{
		std::string errMsg = "Json keys not valid! Json: " + j.dump() + " Expected Keys: username, password.";
		throw std::exception(errMsg.c_str());
	}
}

SignupRequest JsonRequestPacketDeserializer::DeserializeSignupRequest(std::vector<unsigned char> buffer)
{
	if (!isRequestValid(buffer, SIGNUP_REQUEST))
	{
		std::string errMsg = "Signup request not valid!";
		throw std::exception(errMsg.c_str());
	}
	int reqLen = Helper::getMsgLen(buffer);
	int index = 0;
	std::vector<char> charReq;
	while (index < reqLen)
		charReq.push_back((char)buffer[(index++) + 5]);

	std::string toJson(charReq.begin(), charReq.end());
	nlohmann::json j = nlohmann::json::parse(toJson);
	if (j.find("username") != j.end() && j.find("password") != j.end() && j.find("email") != j.end())
		return SignupRequest { j["username"] , j["password"] , j["email"] };
	else
	{
		std::string errMsg = "Json keys not valid! Json: " + j.dump() + " Expected Keys: username, password, email.";
		throw std::exception(errMsg.c_str());
	}
}

bool JsonRequestPacketDeserializer::isRequestValid(std::vector<unsigned char> buffer, int reqType)
{
	int reqLen = Helper::getMsgLen(buffer);
	if (reqLen <= 0)
		return false;

	int reqCode = Helper::getMsgCode(buffer);
	bool validCode = false;
	switch (reqType)
	{
	case LOGIN_REQUEST:
		if (reqCode == LOGIN_REQUEST)
			validCode = true;
		break;
	case SIGNUP_REQUEST:
		if (reqCode == SIGNUP_REQUEST)
			validCode = true;
		break;
	}
	
	return validCode; // Will return true if request code is valid.
}
