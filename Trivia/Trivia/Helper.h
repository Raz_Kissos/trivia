#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>


class Helper
{
public:
	std::string toBinary(int n);
	void pad(char* s, int n, int c);
	static int getTypeCodeFromMessage(SOCKET sc);
	static char* getPartFromSocket(SOCKET sc, int bytesNum);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	static std::vector<unsigned char> getBuffer(SOCKET sc);
	static void sendData(SOCKET sc, std::string message);
	static std::vector<unsigned char> chArrToBuffer(const char* buffer, int len);
	static int getMsgCode(std::vector<unsigned char> buffer);
	static int getMsgLen(std::vector<unsigned char> buffer);
	static std::string BufferToStr(std::vector<unsigned char> buffer);

private:
	static int getDataLengthFromMessage(SOCKET sc);
};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif