#include "RequestHandlerFactory.h"
RequestHandlerFactory* RequestHandlerFactory::instance = 0;

RequestHandlerFactory::RequestHandlerFactory() : _db(new SqliteDataBase()), _loginManager(LoginManager::getInstance())
{
}
RequestHandlerFactory::~RequestHandlerFactory()
{
	delete this->_db;
}
RequestHandlerFactory* RequestHandlerFactory::getInstance(void)
{
	if (instance == nullptr)
		instance = new RequestHandlerFactory();
	return instance;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler();
}

LoginManager* RequestHandlerFactory::getLoginManager()
{
	return this->_loginManager;
}